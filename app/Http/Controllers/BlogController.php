<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Redirect;
use Session;

class BlogController extends Controller
{

    /**
     * @return Show list of all the blog articles which are not deleted, (Latest article first)
     */
    public function showBlogList()
    {
        $articles = Blog::latest()->where('deleted', 0)->get();
        return view('blog_list', compact('articles'));
    }

    /**
     * @return Showing form for creating new blog article
     */
    public function showForm()
    {
        if (Session::get('user_id')) {

            return view('create');
        }

        Session::flash('flash_message', 'Unauthorized Access!');
        return redirect('/login');
    }

    /**
     * @param  Blog ID
     * @return form for editing the existing blog article
     */
    public function editArticle($id)
    {
        $blog = Blog::findOrFail($id);

        if ($blog->user_id == Session::get('user_id') || Session::get('user_role') == 2) {
            return view('create', compact('blog'));
        } else {
            Session::flash('flash_message', 'Unauthorized Access!');
            return redirect('/');
        }
    }

    /**
     * @param  [title, blog body content, blog id]
     * @return [same function is used to create new article as well as updating existing article]
     */
    public function createArticle(Request $request)
    {
        $validatedData = $request->validate([
            'title'   => 'required|min:2|max:255',
            'content' => 'required|min:100|max:1000',
        ]);

        $blog          = Blog::findOrNew($request->id);
        $blog->title   = $request->title;
        $blog->user_id = Session::get('user_id');
        $blog->content = $request->content;
        $blog->save();

        Session::flash('flash_message', 'Success!');

        return redirect('/');

    }

    /**
     * @param  [Blog ID]
     * @return Showing Article reading page where single article gets loaded
     */
    public function showArticle($id)
    {
        $blog = Blog::findOrFail($id);

        if ($blog->deleted == 0) {
            return view('blog_article', compact('blog'));
        }

        Session::flash('flash_message', 'Article is deleted!');
        return redirect('/');
    }

    /**
     * @param  [Blog ID]
     * @return [soft deleting blog article and returning appropriate message]
     */
    public function deleteArticle($id)
    {
        $article = Blog::findOrFail($id)->update(['deleted' => 1]);

        if ($article) {
            Session::flash('flash_message', 'Article Deleted!');
        } else {
            Session::flash('flash_message', 'Error in Deleting Article!');
        }

        return redirect('/');
    }
}
