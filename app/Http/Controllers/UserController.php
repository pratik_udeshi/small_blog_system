<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{

    /**
     * @return Showing login form
     */
    public function showLoginForm()
    {
        return view('login');
    }

    /**
     * @return Showing Registration form
     */
    public function showRegistrationForm()
    {
        return view('register');
    }

    /**
     * @param  [name, email, password, confirm password]
     * @return [registering new user into the system]
     */
    public function registerUser(Request $request)
    {

        $validatedData = $request->validate([
            'name'     => 'required|min:2|max:50',
            'email'    => 'required|email|unique:user',
            'password' => 'required|min:6|confirmed',
        ]);

        $request['password'] = bcrypt($request->password);
        $user_create         = User::create($request->all());

        Session::flash('error_message', 'Success!');

        return redirect('/login');
    }

    /**
     * @param  [email, password]
     * @return [performing login for the user else redirecting back with error]
     */
    public function doLogin(Request $request)
    {
        $validatedData = $request->validate([
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        $user_details = User::where('email', '=', $request->email)->first();

        if (!empty($user_details)) {

            $login = Hash::check($request->password, $user_details->password);

            if ($login == true) {

                // Setting user session when user successfully authenticated
                Session::put('user_login_status', 1);
                Session::put('user_id', $user_details->id);
                Session::put('user_email', $user_details->email);
                Session::put('user_role', $user_details->role);

                return redirect('/');
            }
        }

        return redirect()->back()->withErrors(['Invalid Credentials, Please try again!']);
    }

    /**
     * @return Flusing the session and logging out the user, redirecting back to homepage
     */
    public function logout()
    {
        Session::flush();
        return redirect('/');
    }
}
