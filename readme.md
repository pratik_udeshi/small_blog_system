## Setup processs
- Clone the project
- Import the database (blog.sql) {kept in {project_directory}\database\dump\blog.sql}
- Modify .env settings ([DB_DATABASE = blog, DB_USERNAME = root, DB_PASSWORD = ])
- composer install
- php artisan serve
- Visit the Link [http://localhost:8000/]

## Blog System description

- This is a small blog application where user can read all the articles posted by other users.
- There are two roles in this system. [1. Normal user, 2. Administrator]
- Normal user has rights to create new articles, edit or delete only their own articles.
- While Administrator has rights to create new articles, edit and delete any articles written by any user.

## Registration

- User can register by clicking register button on Homepage. where user must provide name, email, password and confirm password.
- All the above mentioned fields are mandatory. Additionally, user must provide unique email address.

## Login

- User can do login by clicking login button on Homepage. User must authenticate to perform further actions.

- Test Login (["pratik.ude@gmail.com", "123123"])

## Create Article

- Only Authenticated users can create articles by clicking "Create new article" button on Homepage.


## Edit Article

- Only Article owners and Administrator can edit articles by clicking on "Edit" button displaying next to the Article Title.

## Delete Article

- Only Article owner and Administrator can delete articles by clicking on "Delete" button displaying next to the Article Title. Clicking this button ask for confirmation before deleting the article.


## Read Article(s)

- Every user can read the article by vising Root url (Homepage) i.e. [http://localhost:8000/]