@extends('layouts.master')


@section('section')

<div class="container"> 
    @if(!empty($blog->title))
       <h1>{{$blog->title}}</h1> <hr/>
       <p style="overflow-wrap:break-word;"> {{$blog->content}} </p>
    @else
       <h1>No Article Found</h1> <hr/>
    @endif
       <a href="/" class="btn btn-primary">Go Back to Article list</a>
</div>

@endsection
