@extends('layouts.master')

@section('section')
<div class="container">

    <h1>Blog List
        @if(Session::get('user_id'))
        <span><a href="/create" class="btn btn-success">Create New Article</a></span>
            <a href="/logout" class="btn btn-secondary" style="float: right;">logout</a>
        @else
        <span>
            <a href="/login" class="btn btn-info" style="float: right; margin-left: 10px;">Login</a>
            <a href="/register" class="btn btn-success" style="float: right;">Register</a>
        </span>
        @endif
    </h1> <hr/>
</div>

<div class="container">
    <div class="row">
        @foreach($articles as $article)
            <div class="card" style="width: 20rem; margin:10px; 20px;">
              <div class="card-body">
                <h4 class="card-title"><a href="blog/{{$article->id}}">{{$article->title}}</a>
                    @if($article->user_id !== null && $article->user_id == Session::get('user_id') || Session::get('user_role') == 2)
                        <a href="edit/{{$article->id}}" class="btn btn-info pull-right">Edit</a>
                        <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href='{{ url("delete/$article->id") }}'>Delete</a>
                    @endif
                </h4>
                <p class="card-text">{{str_limit($article->content, 50, ' (...)')}}</p>
                <a href="blog/{{$article->id}}" class="btn btn-primary">Read Article..</a>
              </div>
            </div>

        @endforeach
    </div>
</div>

@endsection
