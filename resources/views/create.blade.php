@extends('layouts.master')


@section('section')

<div class="container">

    <h1>Create Blog Post</h1> <hr/>

    <form action="/create" method="post" class="form-horizontal" autocomplete="off">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="id" value="{{ !empty($blog->id) ? $blog->id : "" }}">

        <div class="form-group">
            <label class="control-label col-sm-2" for="title">
                Title:
            </label>
            <div class="col-sm-10">
                <input class="form-control" id="title" name="title" placeholder="Enter Blog Title" value="{{ (!empty($blog->title)) ? $blog->title : Request::old('title')}}" required type="title"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="content">
                Content:
            </label>
            <div class="col-sm-10">
                <textarea class="form-control" id="content" name="content" required rows="5" placeholder="Enter Blog Content">{{ (!empty($blog->content)) ? $blog->content : Request::old('content')}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit">
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>

@endsection
