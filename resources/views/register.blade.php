@extends('layouts.master')


@section('section')

<div class="container">

    <h1>User Registration</h1> <hr/>

    <form action="/register" method="POST" class="form-horizontal" autocomplete="off">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label class="control-label col-sm-2" for="name">
                Name:
            </label>
            <div class="col-sm-10">
                <input class="form-control" id="name" name="name" value="{{ Request::old('name')}}" required type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">
                Email:
            </label>
            <div class="col-sm-10">
                <input class="form-control" id="email" name="email" value="{{ Request::old('email')}}" required type="email"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="password">
                password:
            </label>
            <div class="col-sm-10">
                <input class="form-control" id="password" name="password" required type="password"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="password_confirmation">
                Confirm Password:
            </label>
            <div class="col-sm-10">
                <input class="form-control" id="password_confirmation" name="password_confirmation" required type="password"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit">
                    Register
                </button>
            </div>
        </div>
    </form>
</div>

@endsection
