<?php

Route::get('/', 'BlogController@showBlogList');

Route::get('/register', 'UserController@showRegistrationForm');
Route::post('/register', 'UserController@registerUser');

Route::get('/login', 'UserController@showLoginForm');
Route::post('/login', 'UserController@doLogin');

Route::get('/logout', 'UserController@logout');

Route::get('create', 'BlogController@showForm');
Route::post('create', 'BlogController@createArticle');

Route::get('edit/{id}', 'BlogController@editArticle');
Route::get('delete/{id}', 'BlogController@deleteArticle');

Route::get('blog/{id}', 'BlogController@showArticle');
